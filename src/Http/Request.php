<?php
namespace Estudyer\Wechat\Http;

use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\ResponseInterface;

class Request
{
    /**
     * @var string $accessToken
     * 上传参数url参数中需要AccessToken的值
     */
    protected string $accessToken = '';

    /**
     * @var string|null $tmp
     * 上传或下载临时文件存储标记
     */
    protected string|null $tmp = null;

    /**
     * @var bool $json
     * 设置返回结果body的类型是否为json
     */
    protected bool $json = true;

    /**
     * @var string $accessTokenName
     * 上传参数url参数中需要AccessToken的参数名
     */
    protected string $accessTokenName = 'access_token';

    /**
     * @param string $accessToken
     * @param bool $json
     * @param string $accessTokenName
     */
    public function __construct(string $accessToken = '', bool $json = true, string $accessTokenName = 'access_token')
    {
        $this->setAccessTokenName($accessTokenName);
        $this->setAccessToken($accessToken);
        $this->setJson($json);
    }

    /**
     * 设定参数中的access token值
     *
     * @param string $accessToken
     * @return Request
     */
    public function setAccessToken(string $accessToken): static
    {
        $this->accessToken = $accessToken;

        return $this;
    }

    /**
     * 设定access token所对应的字段名字
     *
     * @param string $accessTokenName
     * @return Request
     */
    public function setAccessTokenName(string $accessTokenName): static
    {
        $this->accessTokenName = $accessTokenName;

        return $this;
    }

    /**
     * 设定是否是json输出
     *
     * @param bool $json
     * @return Request
     */
    public function setJson(bool $json): static
    {
        $this->json = $json;

        return $this;
    }

    /**
     * 获取微信服务器信息
     *
     * @param string $url
     * @param array $params
     * @param array $options
     * @return mixed
     * @throws GuzzleException
     * @throws Exception
     */
    public function get(string $url, array $params = [], array $options = []): mixed
    {
        $client = new Client($options);
        $query = $this->getQueryParam4AccessToken();
        $params = array_merge($params, $query);
        $response = $client->get($url, ['query' => $params]);

        if ($this->isSuccessful($response)) {
            return $this->getJson($response); // $response->json();
        } else {
            throw new Exception('微信服务器未有效的响应请求');
        }
    }

    /**
     * 推送消息给到微信服务器
     *
     * @param string $url
     * @param array $params
     * @param array $options
     * @param string $body
     * @param array $queryParams
     * @return mixed
     * @throws GuzzleException
     * @throws Exception
     */
    public function post(string $url, array $params = [], array $options = [], string $body = '', array $queryParams = []): mixed
    {
        $client = new Client($options);
        $query = $this->getQueryParam4AccessToken();
        if (!empty($queryParams)) $query = array_merge($query, $queryParams);

        $response = $client->post($url, [
            'query' => $query,
            'body' => empty($body) ? json_encode($params, JSON_UNESCAPED_UNICODE) : $body
        ]);

        if ($this->isSuccessful($response)) {
            return $this->getJson($response); // $response->json();
        } else {
            throw new Exception('微信服务器未有效的响应请求');
        }
    }

    /**
     * 上传文件
     *
     * @param string $url
     * @param string $media url或者filepath
     * @param array $options
     * @param array $otherQuery
     * @return mixed
     * @throws GuzzleException
     * @throws Exception
     */
    public function uploadFile(string $url, string $media, array $options = ['fieldName' => 'media'], array $otherQuery = []): mixed
    {
        $client = new Client();
        $query = $this->getQueryParam4AccessToken();
        if (!empty($otherQuery)) $query = array_merge($query, $otherQuery);
        // 远程文件
        if (filter_var($media, FILTER_VALIDATE_URL) !== false) {
            // 远程获取文件名和文件内容
            $fileInfo = $this->getFileByUrl($media);
            // 保存远程文件内容到本地临时文件
            $media = $this->saveAsTemp($fileInfo['name'], $fileInfo['bytes']);
            // 打开本地文件获取资源权柄
            $media = fopen($media, 'r');
        }
        // 本地文件-可读文件
        elseif (is_readable($media)) {
            $media = fopen($media, 'r');
        } else {
            throw new Exception('无效的上传文件');
        }

        $response = $client->post($url, [
            'query' => $query,
            'multipart' => [[
                'name' => $options['fieldName'],
                'contents' => $media
            ]]
        ]);

        if ($this->isSuccessful($response)) {
            return $this->getJson($response); // $response->json();
        } else {
            throw new Exception('微信服务器未有效的响应请求');
        }
    }

    /**
     * 上传多个文件
     *
     * @param string $uri
     * @param array $fileParams
     * @param array $extraParams
     * @param array $description
     * @return mixed
     * @throws Exception|GuzzleException
     */
    public function uploadFiles(string $uri, array $fileParams, array $extraParams = [], array $description = []): mixed
    {
        $client = new Client();

        $files = array();
        foreach ($fileParams as $fileName => $media) {
            if (filter_var($media, FILTER_VALIDATE_URL) !== false) {
                $fileInfo = $this->getFileByUrl($media);
                $media = $this->saveAsTemp($fileInfo['name'], $fileInfo['bytes']);
                $media = fopen($media, 'r');
            } elseif (is_readable($media)) {
                $media = fopen($media, 'r');
            } else {
                throw new Exception('无效的上传文件');
            }
            $files[$fileName] = $media;
        }
        $multipart = array();
        if (!empty($files)) {
            foreach ($files as $field => $value) {
                $multipart[] = ['name' => $field, 'contents' => $value];
            }
        }
        // 如果需要额外的提交参数的话
        $body = '';
        if (!empty($extraParams)) {
            $body = json_encode($extraParams, JSON_UNESCAPED_UNICODE);
        }

        if (!empty($description)) {
            $multipart[] = [
                'name' => 'description',
                'contents' => json_encode($description, JSON_UNESCAPED_UNICODE)
            ];
        }

        $response = $client->post($uri, [
            'query' => ['access_token' => $this->accessToken],
            'multipart' => $multipart,
            'body' => $body
        ]);

        if ($this->isSuccessful($response)) {
            return $this->getJson($response); // $response->json();
        } else {
            throw new Exception('微信服务器未有效的响应请求');
        }
    }

    /**
     * Checks if HTTP Status code is Successful (2xx | 304)
     *
     * @param ResponseInterface $response
     * @return bool
     */
    public function isSuccessful(ResponseInterface $response): bool
    {
        $statusCode = $response->getStatusCode();

        return ($statusCode >= 200 && $statusCode < 300) || $statusCode == 304;
    }

    /**
     * 下载文件
     *
     * @param string $url
     * @param string $file_ext
     * @return array
     */
    public function getFileByUrl(string $url = '', string $file_ext = ''): array
    {
        $opts = [
            'http' => [
                'follow_location' => 3,
                'max_redirects' => 3,
                'timeout' => 10,
                'method' => "GET",
                'header' => "Connection: close\r\n",
                'user_agent' => 'R&D'
            ]
        ];
        $context = stream_context_create($opts); // 创建资源流
        // 获取文件资源内容放入自定义资源流中
        $fileBytes = file_get_contents($url, false, $context);

        // 设置文件后缀
        $ext = $file_ext;
        if (empty($file_ext)) {
            $ext = pathinfo(parse_url($url, PHP_URL_PATH), PATHINFO_EXTENSION);
            if (empty($ext)) $ext = 'jpg';
        }

        return ['name' => uniqid() . '.' . $ext, 'bytes' => $fileBytes];
    }

    /**
     * 将指定文件名和内容的数据，保存到临时文件中，在析构函数中删除临时文件
     *
     * @param string $fileName
     * @param $fileBytes
     * @return string|null
     */
    protected function saveAsTemp(string $fileName, $fileBytes): ?string
    {
        $this->tmp = sys_get_temp_dir() . '/temp_files_' . $fileName;

        file_put_contents($this->tmp, $fileBytes);

        return $this->tmp;
    }

    /**
     * @param $response
     * @return mixed
     */
    protected function getJson($response): mixed
    {
        $body = $response->getBody();
        $contents = $response->getBody()->getContents();
        try {
            if ($this->json) {
                $json = json_decode($body, true);
                if (JSON_ERROR_NONE !== json_last_error()) {
                    throw new \InvalidArgumentException('Unable to parse JSON data: ');
                }
                return $json;
            } else {
                return $contents;
            }
        } catch (Exception $e) {
            return $contents;
        }
    }

    /**
     * @return array
     */
    protected function getQueryParam4AccessToken(): array
    {
        $params = array();
        if (!empty($this->accessTokenName) && !empty($this->accessToken)) {
            $params[$this->accessTokenName] = $this->accessToken;
        }

        return $params;
    }

    public function __destruct()
    {
        if (!empty($this->tmp)) unlink($this->tmp);
    }
}
