<?php
namespace Estudyer\Wechat;

class Config
{
    private array $config = [
        'appid' => '',
        'secret' => '',
        'token' => ''
    ];

    /**
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        $this->setAppID($this->getByKey($config, 'appid'));
        $this->setSecret($this->getByKey($config, 'secret'));
        $this->setToken($this->getByKey($config, 'token'));
    }

    /**
     * @param array $array
     * @param $key
     * @return mixed
     */
    private function getByKey(array $array, $key): mixed
    {
        return array_key_exists($key, $array) ? $array[$key] : null;
    }

    /**
     * @param string $appid
     */
    private function setAppID(string $appid) {
        $this->config['appid'] = $appid;
    }

    /**
     * @param string $secret
     */
    private function setSecret(string $secret) {
        $this->config['secret'] = $secret;
    }

    /**
     * @param string $token
     */
    private function setToken(string $token) {
        $this->config['token'] = $token;
    }

    /**
     * @return string
     */
    public function getAppID(): string
    {
        return $this->config['appid'];
    }

    /**
     * @return string
     */
    public function getSecret(): string
    {
        return $this->config['secret'];
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->config['token'];
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function get(string $key = ''): mixed
    {
        if (empty($key)) return $this->config;

        return $this->getByKey($this->config, $key);
    }
}
