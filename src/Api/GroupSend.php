<?php
namespace Estudyer\Wechat\Api;

use Estudyer\Wechat\MsgCrypt\ErrorCode;
use GuzzleHttp\Exception\GuzzleException;

class GroupSend extends ApiBase
{
    protected string $url = 'https://api.weixin.qq.com/cgi-bin/';

    /**
     * https://developers.weixin.qq.com/doc/offiaccount/Message_Management/Batch_Sends_and_Originality_Checks.html#0
     * 上传图文消息内的图片获取URL
     *
     * @param mixed $media
     * @return array
     * @throws GuzzleException
     */
    public function uploadMedia(mixed $media): array
    {
        $data = $this->request->post($this->url . 'media/uploadimg', ['media' => $media]);
        
        if (!isset($data['url'])) {
            return [ErrorCode::$ResDataAbnormal, $this->error($data)];
        }

        return $this->success($data['url']);
    }

    /**
     * https://developers.weixin.qq.com/doc/offiaccount/Message_Management/Batch_Sends_and_Originality_Checks.html#1
     * 上传图文消息素材
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function uploadNews(array $params): array
    {
        $data = $this->request->post($this->url . 'media/uploadnews', $params);

        if (!isset($data['media_id'])) {
            return [ErrorCode::$ResDataAbnormal, $this->error($data)];
        }

        return $this->success($data);
    }

    /**
     * https://developers.weixin.qq.com/doc/offiaccount/Message_Management/Batch_Sends_and_Originality_Checks.html#2
     * 根据标签进行群发
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function sendByTag(array $params): array
    {
        $data = $this->request->post($this->url . 'message/mass/sendall', $params);
   
        if ($data['errcode'] !== 0) {
            return [ErrorCode::$ResDataAbnormal, $this->error($data)];
        }

        return $this->success($data);
    }

    /**
     * https://developers.weixin.qq.com/doc/offiaccount/Message_Management/Batch_Sends_and_Originality_Checks.html#3
     * 根据 OpenID 列表群发 <[B 订阅号不可用 B]>
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function sendByOpenIDs(array $params): array
    {
        $data = $this->request->post($this->url . 'message/mass/send', $params);

        if ($data['errcode'] !== 0) {
            return [ErrorCode::$ResDataAbnormal, $this->error($data)];
        }

        return $this->success($data);
    }

    /**
     * https://developers.weixin.qq.com/doc/offiaccount/Message_Management/Batch_Sends_and_Originality_Checks.html#4
     * 删除群发
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function delete(array $params): array
    {
        $data = $this->request->post($this->url . 'message/mass/delete', $params);

        if ($data['errcode'] !== 0) {
            return [ErrorCode::$ResDataAbnormal, $this->error($data)];
        }

        return $this->success();
    }

    /**
     * https://developers.weixin.qq.com/doc/offiaccount/Message_Management/Batch_Sends_and_Originality_Checks.html#5
     * 预览接口
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function overview(array $params): array
    {
        $data = $this->request->post($this->url . 'message/mass/preview', $params);

        if ($data['errcode'] !== 0) {
            return [ErrorCode::$ResDataAbnormal, $this->error($data)];
        }

        return $this->success();
    }

    /**
     * https://developers.weixin.qq.com/doc/offiaccount/Message_Management/Batch_Sends_and_Originality_Checks.html#6
     * 查询群发状态
     * @param string $msgID
     * @return array
     * @throws GuzzleException
     */
    public function getSendStatus(string $msgID): array
    {
        $data = $this->request->post($this->url . 'message/mass/get', ['msg_id' => $msgID]);

        if (!isset($data['msg_id'])) {
            return [ErrorCode::$ResDataAbnormal, $this->error($data)];
        }

        return $this->success($data);
    }

    /**
     * https://developers.weixin.qq.com/doc/offiaccount/Message_Management/Batch_Sends_and_Originality_Checks.html#9
     * 获取群发速度
     *
     * @return array
     * @throws GuzzleException
     */
    public function getSpeed(): array
    {
        $data = $this->request->post($this->url . 'message/mass/speed/get');

        if (!isset($data['speed'])) {
            return [ErrorCode::$ResDataAbnormal, $this->error($data)];
        }

        return $this->success($data);
    }

    /**
     * https://developers.weixin.qq.com/doc/offiaccount/Message_Management/Batch_Sends_and_Originality_Checks.html#9
     * 设置群发速度
     *
     * @param int $speed
     * @return array
     * @throws GuzzleException
     */
    public function setSpeed(int $speed): array
    {
        $data = $this->request->post($this->url . 'message/mass/speed/set', ['speed' => $speed]);

        if ($data['errcode'] !== 0) {
            return [ErrorCode::$ResDataAbnormal, $this->error($data)];
        }

        return $this->success();
    }
}
