<?php
namespace Estudyer\Wechat\Api;

trait Handlers
{
    /**
     * https://developers.weixin.qq.com/doc/offiaccount/Basic_Information/Get_access_token.html
     * 获取公众号AccessToken
     *
     * @return AccessToken
     */
    public function accessToken(): AccessToken
    {
        return new AccessToken($this);
    }

    /**
     * https://developers.weixin.qq.com/doc/offiaccount/Basic_Information/Get_the_WeChat_server_IP_address.html
     * 获取微信服务器IP地址列表
     *
     * @return IpManager
     */
    public function getIpManager(): IpManager
    {
        return new IpManager($this);
    }

    /**
     * https://developers.weixin.qq.com/doc/offiaccount/openApi/clear_quota.html
     *
     * @return Quota
     */
    public function getQuota(): Quota
    {
        return new Quota($this);
    }

    /**
     * https://developers.weixin.qq.com/doc/offiaccount/Custom_Menus/Creating_Custom-Defined_Menu.html
     * 菜单管理&个性化菜单管理
     *
     * @return Menu
     */
    public function getMenu(): Menu
    {
        return new Menu($this);
    }

    /**
     * https://developers.weixin.qq.com/doc/offiaccount/Message_Management/Template_Message_Interface.html
     * 模版消息管理
     *
     * @return TemplateMessage
     */
    public function getTemplate(): TemplateMessage
    {
        return new TemplateMessage($this);
    }

    /**
     * https://developers.weixin.qq.com/doc/offiaccount/Message_Management/One-time_subscription_info.html
     * 公众号一次性订阅消息设置
     * https://developers.weixin.qq.com/doc/offiaccount/Subscription_Messages/api.html
     * 订阅通知接口
     *
     * @return Subscribe
     */
    public function getSubscribe(): Subscribe
    {
        return new Subscribe($this);
    }

    /**
     * https://developers.weixin.qq.com/doc/offiaccount/Message_Management/Batch_Sends_and_Originality_Checks.html
     * 群发接口和原创校验 - 已认证的公众号和订阅号可用[部分接口订阅号不能用]
     *
     * @return GroupSend
     */
    public function getGroupSend(): GroupSend
    {
        return new GroupSend($this);
    }

    /**
     * https://developers.weixin.qq.com/doc/offiaccount/Message_Management/Getting_Rules_for_Auto_Replies.html
     * 公众号自动回复规则
     * @return AutoReply
     */
    public function getAutoReply(): AutoReply
    {
        return new AutoReply($this);
    }

    /**
     * 客服管理
     * @return Service
     */
    public function getService(): Service
    {
        return new Service($this);
    }
}
