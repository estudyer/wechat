<?php
namespace Estudyer\Wechat\Api;

use Estudyer\Wechat\MsgCrypt\ErrorCode;
use GuzzleHttp\Exception\GuzzleException;

class AccessToken extends ApiBase
{
    protected string $url = 'https://api.weixin.qq.com/cgi-bin/token';

    /**
     * @return array
     * @throws GuzzleException
     */
    public function get(): array
    {
        $data = $this->request->get($this->url, [
            'grant_type' => 'client_credential',
            'appid' => $this->client->getConfig()->getAppID(),
            'secret' => $this->client->getConfig()->getSecret()
        ]);

        if (!isset($data['access_token'])) {
            return [ErrorCode::$ResDataAbnormal, $this->error($data)];
        }

        return $this->success(['access_token' => $data['access_token'], 'expires_in' => $data['expires_in']]);
    }
}
