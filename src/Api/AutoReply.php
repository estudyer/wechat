<?php
namespace Estudyer\Wechat\Api;

use Estudyer\Wechat\MsgCrypt\ErrorCode;
use GuzzleHttp\Exception\GuzzleException;

class AutoReply extends ApiBase
{
    protected string $url = 'https://api.weixin.qq.com/cgi-bin/';

    /**
     * https://developers.weixin.qq.com/doc/offiaccount/Message_Management/Getting_Rules_for_Auto_Replies.html
     * 获取公众号自动回复规则
     *
     * @return array
     * @throws GuzzleException
     */
    public function getRules(): array
    {
        $data = $this->request->get($this->url . 'get_current_autoreply_info');

        if ($this->getCode($data) > 0) {
            return [ErrorCode::$ResDataAbnormal, $this->error($data)];
        }

        return $this->success($data);
    }
}
