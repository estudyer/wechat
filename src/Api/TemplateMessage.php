<?php
namespace Estudyer\Wechat\Api;

use Estudyer\Wechat\MsgCrypt\ErrorCode;
use GuzzleHttp\Exception\GuzzleException;

/**
 * https://developers.weixin.qq.com/doc/offiaccount/Message_Management/Template_Message_Interface.html
 * 公众号模版消息接口
 */
class TemplateMessage extends ApiBase
{
    protected string $url = 'https://api.weixin.qq.com/cgi-bin/';

    /**
     * https://developers.weixin.qq.com/doc/offiaccount/Message_Management/Template_Message_Interface.html#0
     * 设置所属行业
     *
     * @param ...$industry
     * @return array
     * @throws GuzzleException
     */
    public function setIndustry(...$industry): array
    {
        if (empty($industry)) {
            return [ErrorCode::$ReqParamsAbnormal];
        }

        $params = reset($industry);
        if (!is_array($params)) {
            $params = [];
            foreach ($industry as $index => $item) {
                $params['industry_id' . ($index + 1)] = $item;
            }
        }

        $data = $this->request->post($this->url . 'template/api_set_industry', $params);
   
        if ($this->getCode($data) > 0) {
            return [ErrorCode::$ResDataAbnormal, $this->error($data)];
        }

        return $this->success();
    }

    /**
     * https://developers.weixin.qq.com/doc/offiaccount/Message_Management/Template_Message_Interface.html#1
     * 获取设置的所属行业
     *
     * @return array
     * @throws GuzzleException
     */
    public function getIndustry(): array
    {
        $data = $this->request->get($this->url . 'template/get_industry');

        if ($this->getCode($data) > 0) {
            return [ErrorCode::$ResDataAbnormal, $this->error($data)];
        }

        return $this->success($data);
    }

    /**
     * https://developers.weixin.qq.com/doc/offiaccount/Message_Management/Template_Message_Interface.html#2
     * 获取模版ID [短ID换长ID]
     * @param string $tempShortID
     * @return array
     * @throws GuzzleException
     */
    public function getTemplateID(string $tempShortID): array
    {
        $data = $this->request->post($this->url . 'template/api_add_template', ['template_id_short' => $tempShortID]);

        if ($data['errcode'] !== 0) {
            return [ErrorCode::$ResDataAbnormal, $this->error($data)];
        }

        return $this->success($data['template_id']);
    }

    /**
     * https://developers.weixin.qq.com/doc/offiaccount/Message_Management/Template_Message_Interface.html#3
     * 获取模版列表
     *
     * @return array
     * @throws GuzzleException
     */
    public function getTemplates(): array
    {
        $data = $this->request->get($this->url . 'template/get_all_private_template');

        if ($this->getCode($data) > 0) {
            return [ErrorCode::$ResDataAbnormal, $this->error($data)];
        }

        return $this->success($data);
    }

    /**
     * https://developers.weixin.qq.com/doc/offiaccount/Message_Management/Template_Message_Interface.html#4
     * 删除模版
     *
     * @param string $templateID [长模版ID]
     * @return array
     * @throws GuzzleException
     */
    public function delete(string $templateID): array
    {
        $data = $this->request->post($this->url . 'template/del_private_template', ['template_id' => $templateID]);

        if ($data['errcode'] !== 0) {
            return [ErrorCode::$ResDataAbnormal, $this->error($data)];
        }

        return $this->success();
    }

    /**
     * https://developers.weixin.qq.com/doc/offiaccount/Message_Management/Template_Message_Interface.html#5
     * 发送模版消息
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function send(array $params): array
    {
        $data = $this->request->post($this->url . 'message/template/send', $params);

        if ($data['errcode'] !== 0) {
            return [ErrorCode::$ResDataAbnormal, $this->error($data)];
        }

        return $this->success($data['sgid']);
    }
}
