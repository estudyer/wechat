<?php
namespace Estudyer\Wechat\Api;

use Estudyer\Wechat\MsgCrypt\ErrorCode;
use GuzzleHttp\Exception\GuzzleException;

class Quota extends ApiBase
{
    protected string $url = 'https://api.weixin.qq.com/cgi-bin/';

    /**
     * @return array
     * @throws GuzzleException
     */
    public function clearQuota(): array
    {
        $data = $this->request->post($this->url . 'clear_quota', ['appid' => $this->client->getConfig()->getAppID()]);

        if ($data['errcode'] !== 0) {
            return [ErrorCode::$ResDataAbnormal, $this->error($data)];
        }

        return $this->success();
    }

    /**
     * @param string $cgiPath
     * @return array
     * @throws GuzzleException
     */
    public function queryQuota(string $cgiPath): array
    {
        $data = $this->request->post($this->url . 'openapi/quota/get', [
            'cgi_path' => $cgiPath
        ]);

        if ($data['errcode'] !== 0) {
            return [ErrorCode::$ResDataAbnormal, $this->error($data)];
        }

        return $this->success($data['quota']);
    }

    /**
     * @param string $rid
     * @return array
     * @throws GuzzleException
     */
    public function queryRid(string $rid): array
    {
        $data = $this->request->post($this->url . 'openapi/rid/get', ['rid' => $rid]);

        if ($data['errcode'] !== 0) {
            return [ErrorCode::$ResDataAbnormal, $this->error($data)];
        }

        return $this->success($data['request']);
    }
}
