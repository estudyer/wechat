<?php
namespace Estudyer\Wechat\Api;

use Estudyer\Wechat\MsgCrypt\ErrorCode;
use GuzzleHttp\Exception\GuzzleException;

class IpManager extends ApiBase
{
    protected string $url = 'https://api.weixin.qq.com/cgi-bin/';

    /**
     * 微信API服务接收服务器的IP地址列表
     *
     * @return array
     * @throws GuzzleException
     */
    public function getApiDomainIps(): array
    {
        $data = $this->request->get($this->url . 'get_api_domain_ip');

        if (!isset($data['ip_list'])) {
            return [ErrorCode::$ResDataAbnormal, $this->error($data)];
        }

        return $this->success($data);
    }

    /**
     * 微信回调是发起请求的IP服务器出口IP地址列表
     *
     * @return array
     * @throws GuzzleException
     */
    public function getCallbackIps(): array
    {
        $data = $this->request->get($this->url . 'getcallbackip');

        if (!isset($data['ip_list'])) {
            return [ErrorCode::$ResDataAbnormal, $this->error($data)];
        }

        return $this->success($data);
    }
}
