<?php
namespace Estudyer\Wechat\Api;

use Estudyer\Wechat\MsgCrypt\ErrorCode;
use GuzzleHttp\Exception\GuzzleException;

class Subscribe extends ApiBase
{
    protected string $url = 'https://api.weixin.qq.com/cgi-bin/';

    /**
     * https://developers.weixin.qq.com/doc/offiaccount/Message_Management/One-time_subscription_info.html
     * 公众号一次性订阅消息
     * 第一步：需要用户同意授权，获取一次给用户推送一条订阅模板消息的机会
     * 构建用户访问的授权链接
     *
     * @param array $params
     * @return string
     */
    public function buildUrl(array $params): string
    {
        $url = 'https://mp.weixin.qq.com/mp/subscribemsg?action=get_confirm&appid=%s&redirect_url=%s&reserved=%s&scene=%s&template_id=%s#wechat_redirect';
        ksort($params);
        $values = array_values($params);

        return sprintf($url, ...array_slice($values, 0, 5));
    }
    /**
     * https://developers.weixin.qq.com/doc/offiaccount/Message_Management/One-time_subscription_info.html
     * 公众号一次性订阅消息
     * 第二步：通过 API 推送订阅模板消息给到授权微信用户
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function sendMessage(array $params): array
    {
        $data = $this->request->post($this->url . 'message/template/subscribe', $params);

        if ($data['errcode'] !== 0) {
            return [ErrorCode::$ResDataAbnormal, $this->error($data)];
        }

        return $this->success();
    }

    /**
     * https://developers.weixin.qq.com/doc/offiaccount/Subscription_Messages/api.html#addTemplate%E9%80%89%E7%94%A8%E6%A8%A1%E6%9D%BF
     * 从公共模板库中选用模板，到私有模板库中
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function selectAddTemplate(array $params): array
    {
        $url = 'https://api.weixin.qq.com/wxaapi/newtmpl/addtemplate';
        $data = $this->request->post($url, $params);

        if ($data['errcode'] !== 0) {
            return [ErrorCode::$ResDataAbnormal, $this->error($data)];
        }

        return $this->success($data['priTmplId']);
    }

    /**
     * https://developers.weixin.qq.com/doc/offiaccount/Subscription_Messages/api.html#deleteTemplate%E5%88%A0%E9%99%A4%E6%A8%A1%E6%9D%BF
     * 删除私有模板库中的模板
     *
     * @param string $priTmplID
     * @return array
     * @throws GuzzleException
     */
    public function deleteTemplate(string $priTmplID): array
    {
        $url = 'https://api.weixin.qq.com/wxaapi/newtmpl/deltemplate';
        $data = $this->request->post($url, ['priTmplId' => $priTmplID]);

        if ($data['errcode'] !== 0) {
            return [ErrorCode::$ResDataAbnormal, $this->error($data)];
        }

        return $this->success();
    }

    /**
     * https://developers.weixin.qq.com/doc/offiaccount/Subscription_Messages/api.html#getCategory%E8%8E%B7%E5%8F%96%E5%85%AC%E4%BC%97%E5%8F%B7%E7%B1%BB%E7%9B%AE
     * 获取公众号类目
     *
     * @return array
     * @throws GuzzleException
     */
    public function getCategory(): array
    {
        $url = 'https://api.weixin.qq.com/wxaapi/newtmpl/getcategory';
        $data = $this->request->get($url);

        if ($data['errcode'] !== 0) {
            return [ErrorCode::$ResDataAbnormal, $this->error($data)];
        }

        return $this->success($data['data']);
    }

    /**
     * https://developers.weixin.qq.com/doc/offiaccount/Subscription_Messages/api.html#getPubTemplateKeyWordsById%E8%8E%B7%E5%8F%96%E6%A8%A1%E6%9D%BF%E4%B8%AD%E7%9A%84%E5%85%B3%E9%94%AE%E8%AF%8D
     * 获取模版中的关键词
     *
     * @param string $tid 模版标题ID-可通过接口获取
     * @return array
     * @throws GuzzleException
     */
    public function getTemplateKeywords(string $tid): array
    {
        $url = 'https://api.weixin.qq.com/wxaapi/newtmpl/getpubtemplatekeywords';
        $data = $this->request->get($url, ['tid' => $tid]);

        if ($data['errcode'] !== 0) {
            return [ErrorCode::$ResDataAbnormal, $this->error($data)];
        }

        return $this->success($data);
    }

    /**
     * https://developers.weixin.qq.com/doc/offiaccount/Subscription_Messages/api.html#getPubTemplateTitleList%E8%8E%B7%E5%8F%96%E7%B1%BB%E7%9B%AE%E4%B8%8B%E7%9A%84%E5%85%AC%E5%85%B1%E6%A8%A1%E6%9D%BF
     * 获取类目下公共模版列表
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function getTemplatesByCategory(array $params): array
    {
        $url = 'https://api.weixin.qq.com/wxaapi/newtmpl/getpubtemplatetitles';
        $data = $this->request->get($url, $params);

        if ($data['errcode'] !== 0) {
            return [ErrorCode::$ResDataAbnormal, $this->error($data)];
        }

        return $this->success($data);
    }

    /**
     * https://developers.weixin.qq.com/doc/offiaccount/Subscription_Messages/api.html#getTemplateList%E8%8E%B7%E5%8F%96%E7%A7%81%E6%9C%89%E6%A8%A1%E6%9D%BF%E5%88%97%E8%A1%A8
     * 获取私有模版列表
     *
     * @return array
     * @throws GuzzleException
     */
    public function getTemplates(): array
    {
        $url = 'https://api.weixin.qq.com/wxaapi/newtmpl/gettemplate';
        $data = $this->request->get($url);

        if ($data['errcode'] !== 0) {
            return [ErrorCode::$ResDataAbnormal, $this->error($data)];
        }

        return $this->success($data['data']);
    }

    /**
     * https://developers.weixin.qq.com/doc/offiaccount/Subscription_Messages/api.html#send%E5%8F%91%E9%80%81%E8%AE%A2%E9%98%85%E9%80%9A%E7%9F%A5
     * 发送订阅通知
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function send(array $params): array
    {
        $data = $this->request->post($this->url . 'message/subscribe/bizsend', $params);

        if ($data['errcode'] !== 0) {
            return [ErrorCode::$ResDataAbnormal, $this->error($data)];
        }

        return $this->success();
    }
}
