<?php
namespace Estudyer\Wechat\Api;

use Estudyer\Wechat\MsgCrypt\ErrorCode;
use GuzzleHttp\Exception\GuzzleException;

class Menu extends ApiBase
{
    protected string $url = 'https://api.weixin.qq.com/cgi-bin/';

    /**
     * https://developers.weixin.qq.com/doc/offiaccount/Custom_Menus/Creating_Custom-Defined_Menu.html
     * 创建自定义普通菜单
     *
     * @param array $menu
     * @return array
     * @throws GuzzleException
     */
    public function create(array $menu): array
    {
        $data = $this->request->post($this->url . 'menu/create', $menu);

        if ($data['errcode'] !== 0) {
            return [ErrorCode::$ResDataAbnormal, $this->error($data)];
        }

        return $this->success();
    }

    /**
     * https://developers.weixin.qq.com/doc/offiaccount/Custom_Menus/Querying_Custom_Menus.html
     * 查询所有自定义普通菜单和个性化菜单
     *
     * @return array
     * @throws GuzzleException
     */
    public function get(): array
    {
        $data = $this->request->get($this->url . 'get_current_selfmenu_info');

        if (isset($data['errcode'])) {
            return [ErrorCode::$ResDataAbnormal, $this->error($result)];
        }

        return $this->success($data);
    }

    /**
     * https://developers.weixin.qq.com/doc/offiaccount/Custom_Menus/Deleting_Custom-Defined_Menu.html
     * 删除所有自定义普通菜单和个性化菜单
     *
     * @return array
     * @throws GuzzleException
     */
    public function delete(): array
    {
        $data = $this->request->get($this->url . 'menu/delete');

        if ($data['errcode'] !== 0) {
            return [ErrorCode::$ResDataAbnormal, $this->error($data)];
        }

        return $this->success();
    }

    /**
     * https://developers.weixin.qq.com/doc/offiaccount/Custom_Menus/Personalized_menu_interface.html#0
     * 添加个性化菜单
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function add(array $params): array
    {
        $data = $this->request->post($this->url . 'menu/addconditional', $params);

        if (!isset($data['menuid'])) {
            return [ErrorCode::$ResDataAbnormal, $this->error($data)];
        }

        return $this->success($data);
    }

    /**
     * https://developers.weixin.qq.com/doc/offiaccount/Custom_Menus/Personalized_menu_interface.html#1
     * 删除单个个性化菜单
     *
     * @param string $menuID
     * @return array
     * @throws GuzzleException
     */
    public function remove(string $menuID): array
    {
        $data = $this->request->post($this->url . 'menu/delconditional', ['menuid' => $menuID]);

        if ($data['errcode'] !== 0) {
            return [ErrorCode::$ResDataAbnormal, $this->error($data)];
        }

        return $this->success();
    }

    /**
     * https://developers.weixin.qq.com/doc/offiaccount/Custom_Menus/Personalized_menu_interface.html#2
     * 测试个性化菜单匹配结果
     *
     * @param string $userID 可以是粉丝的OpenID，也可以是粉丝的微信号
     * @return array
     * @throws GuzzleException
     */
    public function match(string $userID): array
    {
        $data = $this->request->post($this->url . 'menu/trymatch', ['user_id' => $userID]);

        if (isset($data['errcode'])) {
            return [ErrorCode::$ResDataAbnormal, $this->error($data)];
        }

        return $this->success($data);
    }
}
