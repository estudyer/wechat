<?php
namespace Estudyer\Wechat\Api\Service;

use Estudyer\Wechat\Api\ApiBase;
use Estudyer\Wechat\MsgCrypt\ErrorCode;
use GuzzleHttp\Exception\GuzzleException;

class CustomerService extends ApiBase
{
    protected string $url = 'https://api.weixin.qq.com/customservice/kfaccount/';

    /**
     * https://developers.weixin.qq.com/doc/offiaccount/Message_Management/Service_Center_messages.html#1
     * 添加客服账号
     * https://developers.weixin.qq.com/doc/offiaccount/Customer_Service/Customer_Service_Management.html#2
     * 添加客服账号
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function create(array $params): array
    {
        $data = $this->request->post($this->url . 'add', $params);

        if ($data['errcode'] !== 0) {
            return [ErrorCode::$ResDataAbnormal, $this->error($data)];
        }

        return $this->success();
    }

    /**
     * https://developers.weixin.qq.com/doc/offiaccount/Message_Management/Service_Center_messages.html#2
     * 修改客服账号
     * https://developers.weixin.qq.com/doc/offiaccount/Customer_Service/Customer_Service_Management.html#4
     * 设置客服账号信息
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function update(array $params): array
    {
        $data = $this->request->post($this->url . 'update', $params);

        if ($data['errcode'] !== 0) {
            return [ErrorCode::$ResDataAbnormal, $this->error($data)];
        }

        return $this->success();
    }

    /**
     * https://developers.weixin.qq.com/doc/offiaccount/Customer_Service/Customer_Service_Management.html#3
     * 邀请绑定客服账号
     *
     * @param string $kfAccount
     * @param string $inviteWx
     * @return array
     * @throws GuzzleException
     */
    public function invite(string $kfAccount, string $inviteWx): array
    {
        $data = $this->request->post($this->url . 'inviteworker', ['kf_account' => $kfAccount, 'invite_wx' => $inviteWx]);

        if ($data['errcode'] !== 0) {
            return [ErrorCode::$ResDataAbnormal, $this->error($data)];
        }

        return $this->success();
    }

    /**
     * https://developers.weixin.qq.com/doc/offiaccount/Message_Management/Service_Center_messages.html#3
     * 删除客服账号
     * https://developers.weixin.qq.com/doc/offiaccount/Customer_Service/Customer_Service_Management.html#6
     * 删除客服账号
     *
     * @param string $kfAccount
     * @return array
     * @throws GuzzleException
     */
    public function delete(string $kfAccount): array
    {
        $data = $this->request->post($this->url . 'del', ['kf_account' => $kfAccount]);

        if ($data['errcode'] !== 0) {
            return [ErrorCode::$ResDataAbnormal, $this->error($data)];
        }

        return $this->success();
    }

    /**
     * https://developers.weixin.qq.com/doc/offiaccount/Message_Management/Service_Center_messages.html#4
     * 设置客服账号头像
     * https://developers.weixin.qq.com/doc/offiaccount/Customer_Service/Customer_Service_Management.html#5
     * 上传客服头像
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function setHeadImage(array $params): array
    {
        $data = $this->request->post($this->url . 'uploadheadimg', $params);

        if ($data['errcode'] !== 0) {
            return [ErrorCode::$ResDataAbnormal, $this->error($data)];
        }

        return $this->success();
    }

    /**
     * https://developers.weixin.qq.com/doc/offiaccount/Message_Management/Service_Center_messages.html#5
     * 获取所有客服账号
     * https://developers.weixin.qq.com/doc/offiaccount/Customer_Service/Customer_Service_Management.html#0
     * 获取客服账号列表
     *
     * @return array
     * @throws GuzzleException
     */
    public function getAccounts(): array
    {
        $url = 'https://api.weixin.qq.com/cgi-bin/customservice/getkflist';
        $data = $this->request->get($url);

        if ($this->getCode($data) > 0) {
            return [ErrorCode::$ResDataAbnormal, $this->error($data)];
        }

        return $this->success($data);
    }
}
