<?php
namespace Estudyer\Wechat\Api\Service;

use Estudyer\Wechat\Api\ApiBase;
use Estudyer\Wechat\MsgCrypt\ErrorCode;
use GuzzleHttp\Exception\GuzzleException;

/**
 * https://developers.weixin.qq.com/doc/offiaccount/Message_Management/Service_Center_messages.html
 * 客服消息管理
 */
class Message extends ApiBase
{
    protected string $url = 'https://api.weixin.qq.com/cgi-bin/';

    /**
     * https://developers.weixin.qq.com/doc/offiaccount/Message_Management/Service_Center_messages.html#7
     * 客服接口-发消息
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function send(array $params): array
    {
        $data = $this->request->post($this->url . 'message/custom/send', $params);

        if ($data['errcode'] !== 0) {
            return [ErrorCode::$ResDataAbnormal, $this->error($data)];
        }

        return $this->success($data);
    }

    /**
     * https://developers.weixin.qq.com/doc/offiaccount/Message_Management/Service_Center_messages.html#%E5%AE%A2%E6%9C%8D%E8%BE%93%E5%85%A5%E7%8A%B6%E6%80%81
     * 客服输入状态
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function typing(array $params): array
    {
        $data = $this->request->post($this->url . 'message/custom/typing', $params);

        if ($data['errcode'] !== 0) {
            return [ErrorCode::$ResDataAbnormal, $this->error($data)];
        }

        return $this->success($data);
    }

    /**
     * https://developers.weixin.qq.com/doc/offiaccount/Customer_Service/Obtain_chat_transcript.html
     * 获取聊天记录
     *
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function records(array $params): array
    {
        $url = 'https://api.weixin.qq.com/customservice/msgrecord/getmsglist';
        $data = $this->request->post($url, $params);

        if ($this->getCode($data) > 0) {
            return [ErrorCode::$ResDataAbnormal, $this->error($data)];
        }

        return $this->success($data);
    }
}
