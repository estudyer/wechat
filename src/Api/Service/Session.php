<?php
namespace Estudyer\Wechat\Api\Service;

use Estudyer\Wechat\Api\ApiBase;
use Estudyer\Wechat\MsgCrypt\ErrorCode;
use GuzzleHttp\Exception\GuzzleException;

/**
 * https://developers.weixin.qq.com/doc/offiaccount/Customer_Service/Session_control.html
 * 客服会话控制
 */
class Session extends ApiBase
{
    protected string $url = 'https://api.weixin.qq.com/customservice/kfsession/';

    /**
     * https://developers.weixin.qq.com/doc/offiaccount/Customer_Service/Session_control.html
     * 创建会话
     *
     * @param string $kfAccount 客服账号
     * @param string $openid 用户openid
     * @return array
     * @throws GuzzleException
     */
    public function create(string $kfAccount, string $openid): array
    {
        $data = $this->request->post($this->url . 'create', ['kf_account' => $kfAccount, 'openid' => $openid]);

        if ($data['errcode'] !== 0) {
            return [ErrorCode::$ResDataAbnormal, $this->error($data)];
        }

        return $this->success();
    }

    /**
     * https://developers.weixin.qq.com/doc/offiaccount/Customer_Service/Session_control.html
     * 关闭会话
     *
     * @param string $kfAccount
     * @param string $openid
     * @return array
     * @throws GuzzleException
     */
    public function close(string $kfAccount, string $openid): array
    {
        $data = $this->request->post($this->url . 'close', ['kf_account' => $kfAccount, 'openid' => $openid]);

        if ($data['errcode'] !== 0) {
            return [ErrorCode::$ResDataAbnormal, $this->error($data)];
        }

        return $this->success();
    }

    /**
     * https://developers.weixin.qq.com/doc/offiaccount/Customer_Service/Session_control.html
     * 获取客服会话列表
     *
     * @param string $kfAccount
     * @return array
     * @throws GuzzleException
     */
    public function sessions(string $kfAccount): array
    {
        $data = $this->request->get($this->url . 'getsessionlist', ['kf_account' => $kfAccount]);

        if ($this->getCode($data) > 0) {
            return [ErrorCode::$ResDataAbnormal, $this->error($data)];
        }

        return $this->success($data);
    }

    /**
     * https://developers.weixin.qq.com/doc/offiaccount/Customer_Service/Session_control.html
     * 获取未接入会话列表
     *
     * @return array
     * @throws GuzzleException
     */
    public function waits(): array
    {
        $data = $this->request->get($this->url . 'getwaitcase');

        if ($this->getCode($data) > 0) {
            return [ErrorCode::$ResDataAbnormal, $this->error($data)];
        }

        return $this->success($data);
    }
}
