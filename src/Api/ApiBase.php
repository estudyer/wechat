<?php
namespace Estudyer\Wechat\Api;

use Estudyer\Wechat\Client;
use Estudyer\Wechat\Http\Request;
use Estudyer\Wechat\MsgCrypt\ErrorCode;

class ApiBase
{
    protected string $url = '';

    protected Client $client;

    protected Request $request;

    public function __construct(Client $client)
    {
        $this->client = $client;
        $this->request = $client->getRequest();
    }

    /**
     * @param array $data
     * @return int
     */
    protected function getCode(array $data): int
    {
        if (!array_key_exists('errcode', $data)) return -10000;

        return $data['errcode'];
    }

    /**
     * @param array $res
     * @return string
     */
    protected function error(array $res): string
    {
        $tpl = '%s[%d] %s';

        return sprintf($tpl, date('Y-m-d H:i:s'), $res['errcode'], $res['errmsg']);
    }

    /**
     * @param mixed $data
     * @return array
     */
    protected function success(mixed $data = []): array
    {
        return [ErrorCode::$OK, $data];
    }
}
