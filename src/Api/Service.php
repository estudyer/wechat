<?php
namespace Estudyer\Wechat\Api;

use Estudyer\Wechat\Api\Service\{CustomerService, Message, Session};
use Estudyer\Wechat\Client;

class Service
{
    private Client $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * 客服账号管理
     *
     * @return CustomerService
     */
    public function getAccountManage(): CustomerService
    {
        return new CustomerService($this->client);
    }

    /**
     * @description 客服消息
     *
     * @return Message
     */
    public function getMessage(): Message
    {
        return new Message($this->client);
    }

    /**
     * https://developers.weixin.qq.com/doc/offiaccount/Customer_Service/Session_control.html
     * 客服会话管理
     *
     * @return Session
     */
    public function getSession(): Session
    {
        return new Session($this->client);
    }
}
