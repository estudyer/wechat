<?php
namespace Estudyer\Wechat;

use Estudyer\Wechat\Api\Handlers;
use Estudyer\Wechat\Http\Request;
use Estudyer\Wechat\MsgCrypt\WXBizMsgCrypt;
use Exception;

class Client
{
    use Handlers;

    private Config|null $config;

    private string $accessToken = '';

    private ?Request $request = null;

    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    /**
     * @param string $accessToken
     * @return $this
     */
    public function setAccessToken(string $accessToken): static
    {
        $this->accessToken = $accessToken;

        return $this;
    }

    /**
     * @return string
     */
    public function getAccessToken(): string
    {
        return $this->accessToken;
    }

    /**
     * @return Config|null
     */
    public function getConfig(): ?Config
    {
        return $this->config;
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function getConfigKey(string $key = ''): mixed
    {
        return $this->config->get($key);
    }

    private function instanceRequest() {
        $this->request = new Request($this->getAccessToken());
    }

    /**
     * @return ?Request
     */
    public function getRequest(): ?Request
    {
        if (empty($this->request)) $this->instanceRequest();

        return $this->request;
    }

    /**
     * 签名校验
     *
     * @param string $token
     * @param string $encodingAesKey
     * @return array|bool
     */
    public function checkSignature(string $token, string $encodingAesKey = ''): array|bool
    {
        $token = trim($token);
        $signature = isset($_GET['msg_signature']) ? trim($_GET['msg_signature']) : '';
        $timestamp = isset($_GET['timestamp']) ? trim($_GET['timestamp']) : '';
        $nonce = isset($_GET['nonce']) ? trim($_GET['nonce']) : '';
        $echostr = isset($_GET['echostr']) ? trim($_GET['echostr']) : '';

        // 需要返回的明文
        $sEcsReplyEchoStrhoStr = '';
        $wxcpt = new WXBizMsgCrypt($token, $encodingAesKey, $this->config->get('appid'));
        $errCode = $wxcpt->VerifyURL($signature, $timestamp, $nonce, $echostr, $sEcsReplyEchoStrhoStr);

        if ($errCode == 0) {
            // 验证URL成功，将sEchoStr返回
            return ['replyEchoStr' => $sEcsReplyEchoStrhoStr];
        } else {
            return false;
        }
    }

    /**
     * 有效性校验
     * @param string $token
     * @param string $encodingAesKey
     * @throws Exception
     */
    public function verify(string $token, string $encodingAesKey = ''): void
    {
        if (empty($token)) throw new Exception('请设定校验签名所需的token');

        $ret = $this->checkSignature($token, $encodingAesKey);

        if (!empty($ret)) exit($ret['replyEchoStr']);
    }

    /**
     * 标准化处理微信的返回结果
     * @param $rst
     * @return mixed
     */
    public function rst($rst): mixed
    {
        return $rst;
    }
}
